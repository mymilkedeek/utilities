package net.mymilkedeek.sandbox.itext;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class HelloWorld {

    public static void main(String[] args) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("src/main/resources/results/pdf/itext_helloworld.pdf"));
        document.open();
        document.add(new Paragraph("Hello World"));
        document.close();
    }
}
