package net.mymilkedeek.sandbox.itext;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class HundredPagedPdf {

    public static void main(String[] args) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("src/main/resources/results/pdf/itext_hundredpages.pdf"));

        document.open();

        for ( int i = 1; i < 101; i++) {
            document.add(new Paragraph("Page " + i));
            document.newPage();
        }

        document.close();
    }
}
