package net.mymilkedeek.dynamicscripting;

import java.util.Map;

public interface EekScript {
    
    public void execute(Map<String, Object> context);
    
}