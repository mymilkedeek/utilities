package net.mymilkedeek.dynamicscripting;

import java.io.IOException;

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */

public class Main {

    public static void main(String[] args) throws IOException, InstantiationException, IllegalAccessException {
        ScriptLoader.run("HelloScript");
        ScriptLoader.run("Addition");
    }

}
