package net.mymilkedeek.dynamicscripting;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */

public class ScriptLoader {

    public static void run(String name) throws IOException, IllegalAccessException, InstantiationException {
        ClassLoader parent = Main.class.getClassLoader();
        GroovyClassLoader loader = new GroovyClassLoader(parent);
        Class groovyClass = loader.parseClass(new File("src/main/groovy/net/mymilkedeek/scripts/" + name + ".groovy"));

        GroovyObject groovyObject = (GroovyObject) groovyClass.newInstance();
        String a = "world";
        Map<String, Object> context = new HashMap<String, Object>();
        context.put("name", "world");

        groovyObject.invokeMethod("execute", context);
    }

}