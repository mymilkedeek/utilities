package net.mymilkedeek.scripts

import net.mymilkedeek.dynamicscripting.EekScript

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class Addition implements EekScript {

    @Override
    void execute (Map<String, Object> context) {
        println "2 + 2 is " + ( 2 + 2 )
    }
}
