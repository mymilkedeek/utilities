package net.mymilkedeek.scripts

import net.mymilkedeek.dynamicscripting.EekScript

class HelloScript implements EekScript {

    @Override
    void execute (Map<String, Object> context) {
        def object = context.get("name")
        println "hello $object"
    }

}