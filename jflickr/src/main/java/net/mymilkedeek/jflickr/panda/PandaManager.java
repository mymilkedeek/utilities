package net.mymilkedeek.jflickr.panda;

import org.json.JSONObject;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import net.mymilkedeek.jflickr.core.Manager;

public class PandaManager extends Manager {

	public PandaManager(OAuthService oAuthService, Token accessToken) {
		super(oAuthService, accessToken);
	}
	
	public Response getPandaPictures(String pandaName) {
		return getResponse("&method=flickr.panda.getPhotos&panda_name=" + pandaName.replaceAll(" ", "+"));
	}
	
	public JSONObject getPandaPicturesAsJSON(String pandaName) {
		return new JSONObject(getPandaPictures(pandaName));
	}
}