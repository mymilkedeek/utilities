package net.mymilkedeek.jflickr;

import net.mymilkedeek.jflickr.oauth.FlickrApi;
import net.mymilkedeek.jflickr.photos.PhotoManager;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

/**
 * Main access point for the library. Get your managers here and authorize your application!
 * 
 * @author MyMilkedEek
 */
public class Flickr {

	private Token requestToken;
	private Token accessToken;
	private OAuthService oAuthService;

	public Flickr(String apiKey, String secret) {
		oAuthService = new ServiceBuilder().provider(FlickrApi.class)
				.apiKey(apiKey).apiSecret(secret).build();
		requestToken = oAuthService.getRequestToken();
	}

	/*
	 * Methods
	 */
	/*
	 * oAuth Methods
	 */
	/**
	 * Get the url to authorize the application. Navigate your user to this url. Upon user input (your key) call {@link Flickr#verifyUser(String)}.
	 * 
	 * @return String url to authorize the application.
	 */
	public String getAuthorizationUrl() {
		return oAuthService.getAuthorizationUrl(requestToken);
	}

	/**
	 * Call after receiving the input the user gives you after being directed to the url provided by {@link Flickr#getAuthorizationUrl()}.
	 * 
	 * @param verifier String
	 */
	public void verifyUser(String verifier) {
		if (accessToken == null) {
			Verifier v = new Verifier(verifier);
			accessToken = oAuthService.getAccessToken(requestToken, v);
		}
	}
	
	/*
	 * API Wrappers
	 */
	/**
	 * Returns a PhotoManager object which holds general photo api methods.
	 * 
	 * @return {@link PhotoManager}
	 */
	public PhotoManager getPhotoManager() {
		return new PhotoManager(oAuthService, accessToken);
	}

	/**
	 * Calls the test login api method.
	 */
	public void testConnection() {
		OAuthRequest request = new OAuthRequest(Verb.GET,
				"http://api.flickr.com/services/rest/?method=flickr.test.login");
		oAuthService.signRequest(accessToken, request);
		request.send();
	}	
}