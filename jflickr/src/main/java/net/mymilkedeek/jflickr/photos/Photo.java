package net.mymilkedeek.jflickr.photos;

public class Photo {
	/* Photo sizes ids */
	public static final int SQUARE = 0;
	private String squareAppendix = "_s";
	public static final int THUMBNAIL = 1;
	private String thumbnailAppendix = "_t";
	public static final int SMALL = 2;
	private String smallAppendix = "_m";
	public static final int MEDIUM = 3;
	public static final int MEDIUM_64 = 4;
	private String medium64Appendix = "_z";
	public static final int LARGE = 5;
	private String largeAppendix = "_b";
	private String extension;

	private String id;
	private String ownerId;
	private String title;
	private String url;

	public Photo(String id, String ownerId, String title) {
		this.id = id;
		this.ownerId = ownerId;
		this.title = title;
	}

	/*
	 * Methods
	 */
	public String getUrl(int size) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(url);

		switch (size) {
		case SQUARE:
			stringBuilder.append(squareAppendix);
			break;
			
		case THUMBNAIL:
			stringBuilder.append(thumbnailAppendix);
			break;
			
		case SMALL:
			stringBuilder.append(smallAppendix);
			break;
			
		case MEDIUM_64:
			stringBuilder.append(medium64Appendix);
			break;
			
		case LARGE:
			stringBuilder.append(largeAppendix);
			break;			
			
		case MEDIUM:
			// fallthrough
		default:
			// do nothing
			break;
		}

		stringBuilder.append(".");
		stringBuilder.append(extension);
		return stringBuilder.toString();
	}

	/*
	 * Getters and Setters
	 */
	public void setUrl(String url) {
		int extensionIndex = url.lastIndexOf(".");
		this.url = url.substring(0, extensionIndex);
		this.extension = url.substring(extensionIndex+1);
	}

	public String getExtension() {
		return extension;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}