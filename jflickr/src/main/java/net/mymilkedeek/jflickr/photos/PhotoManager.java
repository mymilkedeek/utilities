package net.mymilkedeek.jflickr.photos;

import java.util.List;

import net.mymilkedeek.jflickr.core.Manager;

import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

public class PhotoManager extends Manager {

	public PhotoManager(OAuthService oAuthService, Token accessToken) {
		super(oAuthService, accessToken);
	}

	public List<Photo> getUserPhotos() {
		Response response = getResponse("&method=flickr.people.getPhotos&user_id=me");
		List<Photo> photos = null;
		try {
			photos = jsonParser.getPhotoListFromJSONObject(new JSONObject(response.getBody()));
			for ( Photo photo : photos ) {
				response = getResponse("&method=flickr.photos.getSizes&photo_id=" + photo.getId());
				jsonParser.addUrlsToPhoto(photo, new JSONObject(response.getBody()));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return photos;
	}
}