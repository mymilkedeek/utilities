package net.mymilkedeek.jflickr.json;

import java.util.ArrayList;
import java.util.List;

import net.mymilkedeek.jflickr.photos.Photo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {
	public Photo getPhotoFromJSONObject(JSONObject jsonPhoto) throws JSONException {
		String id = jsonPhoto.getString("id");
		String ownerId = jsonPhoto.getString("owner");
		String title = jsonPhoto.getString("title");
		return new Photo(id, ownerId, title);
	}

	public List<Photo> getPhotoListFromJSONObject(JSONObject jsonPhotos) throws JSONException {
		List<Photo> list = new ArrayList<Photo>();
		
		System.out.println(jsonPhotos.toString());
		
		JSONArray photoArray = jsonPhotos.getJSONObject("photos").optJSONArray("photo");
		
		for ( int i = 0; i < photoArray.length(); i++ ) {
			list.add(getPhotoFromJSONObject(photoArray.getJSONObject(i)));
		}
		
		return list;
	}
	
	public void addUrlsToPhoto(Photo photo, JSONObject jsonUrls) throws JSONException {
		JSONArray urlArray = jsonUrls.getJSONObject("sizes").optJSONArray("size");
		
		for ( int i = 0; i < urlArray.length(); i++) {
			JSONObject o = urlArray.getJSONObject(i);
			if ( o.getString("label").equalsIgnoreCase("medium")) {
				photo.setUrl(o.getString("source"));
			}
		}
	}
}