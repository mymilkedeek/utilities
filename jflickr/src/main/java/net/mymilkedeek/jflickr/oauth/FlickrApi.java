package net.mymilkedeek.jflickr.oauth;

import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.Token;

public class FlickrApi extends DefaultApi10a {

	@Override
	public String getAccessTokenEndpoint() {
		return "http://www.flickr.com/services/oauth/access_token";
	}

	@Override
	public String getAuthorizationUrl(Token token) {
		return "http://www.flickr.com/services/oauth/authorize?oauth_token="
				+ token.getToken();
	}

	@Override
	public String getRequestTokenEndpoint() {
		return "http://www.flickr.com/services/oauth/request_token";
	}
}