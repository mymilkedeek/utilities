package net.mymilkedeek.jflickr;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Scanner;

import net.mymilkedeek.jflickr.photos.Photo;
import net.mymilkedeek.jflickr.photos.PhotoManager;

public class Main {
	
	private static final String apiKey = "7935a7addd92280a3d2fb6655d86fc42";
	private static final String secret = "26ca03703cfffa06";
	
	public static void main(String[] args) {
		Flickr flickr = new Flickr(apiKey, secret);
		String url = flickr.getAuthorizationUrl();
		Desktop desktop = Desktop.getDesktop();
		try {
			desktop.browse(new URI(url));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}		
		Scanner s = new Scanner(System.in);
		flickr.verifyUser(s.next());
		
		PhotoManager photoManager = flickr.getPhotoManager();
		List<Photo> photos = photoManager.getUserPhotos();
		for ( Photo p : photos ) {
			System.out.println(p.getUrl(Photo.SQUARE));
		}
	}
}