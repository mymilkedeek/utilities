package net.mymilkedeek.jflickr.core;

import net.mymilkedeek.jflickr.json.JSONParser;

import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

public class Manager {
	private static final String apiUrl = "http://api.flickr.com/services/rest?nojsoncallback=1&format=json";
	private OAuthService oAuthService;
	private Token accessToken;
	protected JSONParser jsonParser;

	public Manager(OAuthService oAuthService, Token accessToken) {
		this.oAuthService = oAuthService;
		this.accessToken = accessToken;
		this.jsonParser = new JSONParser();
	}

	/**
	 * Get the response from the flickr api as a Response from the scribe library.
	 *
	 * @return org.scribe.model.Response
	 */
	public Response getResponse(String apiCall) {
		OAuthRequest request = new OAuthRequest(Verb.GET, apiUrl + apiCall);
		oAuthService.signRequest(accessToken, request);
		return request.send();
	}
	
	/**
	 * Get the response from the flickr api as a JSON Object.
	 * 
	 * @param apiCall
	 * @return
	 * @throws JSONException
	 */
	public JSONObject getJSONResponse(String apiCall) throws JSONException {
		return new JSONObject(getResponse(apiCall).getBody());
	}
}