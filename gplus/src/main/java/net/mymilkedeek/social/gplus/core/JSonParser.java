package net.mymilkedeek.social.gplus.core;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

public interface JSonParser<E> {
	public E parseJSon(JSONObject json) throws JSONException;
	public List<E> parseJSonList(JSONObject json) throws JSONException;
}
