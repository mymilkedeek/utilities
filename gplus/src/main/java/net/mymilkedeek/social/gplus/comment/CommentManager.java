package net.mymilkedeek.social.gplus.comment;

import java.util.List;

import org.json.JSONException;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import net.mymilkedeek.social.gplus.GooglePlusException;
import net.mymilkedeek.social.gplus.core.JSonParser;
import net.mymilkedeek.social.gplus.core.Manager;

public class CommentManager extends Manager<Comment> {

	public CommentManager(OAuthService oAuthService, Token accessToken,
			JSonParser<Comment> jsonParser) {
		super(oAuthService, accessToken, jsonParser);
	}

	public Comment getComment(String commentId) throws GooglePlusException {
		Comment comment = null;
		
		try {
			comment = getParsedJSONObject("comments/" + commentId);;
		} catch ( JSONException jsonException ) {
			throw new GooglePlusException("Error while fetching comment with id :" + commentId);
		}
		
		return comment;
	}
	
	public List<Comment> findCommentsByActivityId(String activityId) throws GooglePlusException {
		List<Comment> comments = null;
		
		try {
			comments = getListFromJSONObject("activities/" + activityId + "/comments");
		} catch (JSONException e) {
			throw new GooglePlusException(e, "Error fetching comments for activity id : " + activityId);
		}
		
		return comments;
	}	
}