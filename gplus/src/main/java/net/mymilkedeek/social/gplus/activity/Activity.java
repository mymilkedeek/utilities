package net.mymilkedeek.social.gplus.activity;

import org.joda.time.DateTime;

import net.mymilkedeek.social.gplus.person.Person;

public class Activity {

	private String id;
	private String title;
	private Person author;
	private DateTime publishedDate;

	public Activity(String id, String title, Person author, DateTime published) {
		this.id = id;
		this.title = title;
		this.author = author;
		this.publishedDate = published;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}
	
	public Person getAuthor() {
		return author;
	}
	
	public DateTime getPublishedDate() {
		return publishedDate;
	}

	@Override
	public String toString() {
		return "Activity [id=" + id + ", title=" + title + ", authorId=" + author.getId() + ", authorDisplayName=" + author.getDisplayName() + ", date=" + publishedDate + "]";
	}
}