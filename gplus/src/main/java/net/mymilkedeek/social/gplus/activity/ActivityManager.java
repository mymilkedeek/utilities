package net.mymilkedeek.social.gplus.activity;

import java.util.List;

import net.mymilkedeek.social.gplus.GooglePlusException;
import net.mymilkedeek.social.gplus.core.JSonParser;
import net.mymilkedeek.social.gplus.core.Manager;

import org.json.JSONException;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

public class ActivityManager extends Manager<Activity> {
	public ActivityManager(OAuthService oAuthService, Token accessToken, JSonParser<Activity> jsonParser) {
		super(oAuthService, accessToken, jsonParser);
	}
	
	public List<Activity> getActivitiesFromAuthorizedUser() throws GooglePlusException {
		return getActivitiesFromUser("me");
	}
	
	public List<Activity> getActivitiesFromUser(String userId) throws GooglePlusException {
		List<Activity> activities = null;
		
		try {
			activities = getListFromJSONObject("people/" + userId + "/activities/public");
		} catch (JSONException e) {
			throw new GooglePlusException(e, "Error while fetching activities from user " + userId);
		}
		
		return activities;
	}

	public Activity getActivity(String activityId) throws GooglePlusException {
		Activity activity = null;
		
		try {
			activity = getParsedJSONObject("activities/"  + activityId + "?alt=json");
		} catch (JSONException e) {
			throw new GooglePlusException(e, "Error while fetching activity with id " + activityId);
		}
		
		return activity;
	}
	
	public List<Activity> findActivity(String query) throws GooglePlusException {
		List<Activity> activities = null;
		
		try {
			activities = getListFromJSONObject("activities?query=" + query);
		} catch (JSONException e) {
			throw new GooglePlusException(e, "Error while querying for an activity whit query " + query);
		}
		
		return activities;
	}
}