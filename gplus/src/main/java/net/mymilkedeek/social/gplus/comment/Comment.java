package net.mymilkedeek.social.gplus.comment;

public class Comment {
	private String commentId;
	private String content;
	
	public Comment(String commentId, String content) {
		super();
		this.commentId = commentId;
		this.content = content;
	}

	public String getCommentId() {
		return commentId;
	}

	public String getContent() {
		return content;
	}

	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", content=" + content + "]";
	}
}