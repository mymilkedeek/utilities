package net.mymilkedeek.social.gplus.json;

import java.util.ArrayList;
import java.util.List;

import net.mymilkedeek.social.gplus.activity.Activity;
import net.mymilkedeek.social.gplus.core.JSonParser;
import net.mymilkedeek.social.gplus.person.Person;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ActivityJsonParser implements JSonParser<Activity> {

	public Activity parseJSon(JSONObject json) throws JSONException {
		JSONObject author = json.getJSONObject("actor");
		JSONObject img    = author.getJSONObject("image");
		return new Activity(json.getString("id"), json.getString("title"), new Person(author.getString("id"), author.getString("displayName"), img.getString("url")), DateParser.parseDate(json.getString("published")));
	}

	public List<Activity> parseJSonList(JSONObject json) throws JSONException {
		JSONArray jsonArray = json.optJSONArray("items");
		List<Activity> activities = new ArrayList<Activity>();
		JSONObject jsonObject;
		
		for ( int i = 0; i < jsonArray.length(); i++) {
			jsonObject = jsonArray.getJSONObject(i);
			activities.add(parseJSon(jsonObject));
		}
		
		return activities;
	}
}