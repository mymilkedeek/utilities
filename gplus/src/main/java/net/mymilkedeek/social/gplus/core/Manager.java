package net.mymilkedeek.social.gplus.core;

import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import java.util.List;

public class Manager<E> {
	private static final String apiUrl = "https://www.googleapis.com/plus/v1/";
	private OAuthService oAuthService;
	private Token accessToken;
	protected JSonParser<E> jsonParser;
	
	public Manager(OAuthService oAuthService, Token accessToken, JSonParser<E> jsonParser) {
		this.oAuthService = oAuthService;
		this.accessToken = accessToken;
		this.jsonParser = jsonParser;
	}

	/**
	 * Get the response from the api as a Response from the scribe library.
	 *
	 * @return org.scribe.model.Response
	 */
	public Response getResponse(String apiCall) {
		OAuthRequest request = new OAuthRequest(Verb.GET, apiUrl + apiCall);
		oAuthService.signRequest(accessToken, request);
		Response response = request.send();
		return response;
	}
	
	/**
	 * Get the response from the api as a JSON Object.
	 * 
	 * @param apiCall
	 * @return
	 * @throws JSONException
	 */
	public JSONObject getJSONResponse(String apiCall) throws JSONException {
		return new JSONObject(getResponse(apiCall).getBody());
	}
	
	public E getParsedJSONObject(String apiCall) throws JSONException {
		return jsonParser.parseJSon(getJSONResponse(apiCall));
	}
	
	public List<E> getListFromJSONObject(String apiCall) throws JSONException {
		return jsonParser.parseJSonList(getJSONResponse(apiCall));
	}
}