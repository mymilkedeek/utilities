package net.mymilkedeek.social.gplus.json;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.mymilkedeek.social.gplus.comment.Comment;
import net.mymilkedeek.social.gplus.core.JSonParser;

public class CommentJsonParser implements JSonParser<Comment> {

	public Comment parseJSon(JSONObject json) throws JSONException {
		return new Comment(json.getString("id"), json.getJSONObject("object").getString("content"));
	}

	public List<Comment> parseJSonList(JSONObject json) throws JSONException {
		JSONArray jsonArray = json.optJSONArray("items");
		List<Comment> comments = new ArrayList<Comment>();
		
		JSONObject jsonObject;
		
		for ( int i = 0; i < jsonArray.length(); i++) {
			jsonObject = jsonArray.getJSONObject(i);
			comments.add(parseJSon(jsonObject));
		}		
		
		return comments;
	}
}