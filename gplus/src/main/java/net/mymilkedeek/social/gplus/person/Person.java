package net.mymilkedeek.social.gplus.person;

public class Person {

	/*
	 * Fields
	 */
	private String id;
	private String displayName;
	private String familyName;
	private String givenName;
	private String gender;
	private String aboutMe;
	private String tagline;
	private String imageUrl;

	public Person() {
		// Empty body
	}

	public Person(String id, String displayName) {
		this.id = id;
		this.displayName = displayName;
	}

	public Person(String id, String displayName, String imgUrl) {
		this(id, displayName);
		this.imageUrl = imgUrl;
	}

	/*
	 * Getters and Setters
	 */
	public String getName() {
		return this.givenName + " " + this.familyName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public String getTagline() {
		return tagline;
	}

	public void setTagline(String tagline) {
		this.tagline = tagline;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", displayName=" + displayName
				+ ", familyName=" + familyName + ", givenName=" + givenName
				+ ", gender=" + gender + ", aboutMe=" + aboutMe + "]";
	}
}