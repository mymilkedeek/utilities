package net.mymilkedeek.social.gplus;

public class GooglePlusException extends Exception {
	private static final long serialVersionUID = 6292639785984107247L;

	public GooglePlusException(String message, String... searchQueries) {
		super(message + getQueries(searchQueries));
	}
	
	public GooglePlusException(Throwable exception, String message) {
		super(message, exception);
	}
	
	public GooglePlusException(Throwable exception, String message, String... searchQueries) {
		super(message + getQueries(searchQueries), exception);
	}

	private static String getQueries(String... queries) {
		StringBuilder builder = new StringBuilder();
		
		builder.append("\nYou could try following Google queries:\n");
		
		for (String s : queries) {
			builder.append(s).append("\n");
		}
		
		return builder.toString();
	}
}
