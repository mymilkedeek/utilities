package net.mymilkedeek.social.gplus.person;

import java.util.List;

import net.mymilkedeek.social.gplus.GooglePlusException;
import net.mymilkedeek.social.gplus.core.JSonParser;
import net.mymilkedeek.social.gplus.core.Manager;

import org.json.JSONException;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

public class PersonManager extends Manager<Person> {
	
	public static String PLUSONERS = "plusoners";
	public static String RESHARERS = "resharers";
	
	public PersonManager(OAuthService oAuthService, Token accessToken, JSonParser<Person> jsonParser) {
		super(oAuthService, accessToken, jsonParser);
	}

	public Person getAuthorizedUser() throws GooglePlusException {
		return getUser("me");
	}
	
	public Person getUser(String id) throws GooglePlusException {
		Person person = null;
		
		try {
			person = getParsedJSONObject("people/" + id);
		} catch (JSONException e) {
			throw new GooglePlusException(e, "Error while fetching user " + id);
		}
		
		return person;
	}
	
	public List<Person> findUser(String query) throws GooglePlusException {
		List<Person> persons = null;
		
		try {
			persons = getListFromJSONObject("people?query=" + query);
		} catch (JSONException e) {
			throw new GooglePlusException(e, "Error while querying a list of users by query " + query);
		}
		
		return persons;
	}
	
	public List<Person> findByActivityId(String activityId, String peopleType) throws GooglePlusException {
		List<Person> persons = null;
		
		try {
			persons = getListFromJSONObject("activities/" + activityId + "/people/" + peopleType);
		} catch (JSONException e) {
			throw new GooglePlusException(e, "Error while fetching a list of users with activity " + activityId);
		}
		
		return persons;
	}
}