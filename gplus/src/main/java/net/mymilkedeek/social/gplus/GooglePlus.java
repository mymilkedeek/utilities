package net.mymilkedeek.social.gplus;

import net.mymilkedeek.social.gplus.activity.ActivityManager;
import net.mymilkedeek.social.gplus.comment.CommentManager;
import net.mymilkedeek.social.gplus.json.ActivityJsonParser;
import net.mymilkedeek.social.gplus.json.CommentJsonParser;
import net.mymilkedeek.social.gplus.json.PersonJSonParser;
import net.mymilkedeek.social.gplus.person.PersonManager;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.GoogleApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

/**
 * Entry point of the api. Get authorization and your managers in this class.
 *
 * @author My Milked Eek (Michael Demey)
 */
public class GooglePlus {

	private Token requestToken;
	private Token accessToken;
	private OAuthService oAuthService;
	
	public GooglePlus(String apiKey, String secret) {
		oAuthService = new ServiceBuilder().provider(GoogleApi.class)
				.apiKey(apiKey).apiSecret(secret).scope("https://www.googleapis.com/auth/plus.me").build();
		requestToken = oAuthService.getRequestToken();
	}

	public String getAuthorizationUrl() {
		return oAuthService.getAuthorizationUrl(requestToken);
	}

	public void verifyUser(String verifier) {
		if (accessToken == null) {
			Verifier v = new Verifier(verifier);
			accessToken = oAuthService.getAccessToken(requestToken, v);
		}
	}	

	/**
	 * If something goes wrong you'll hear about it.
	 */
	public void testConnection() {
		OAuthRequest request = new OAuthRequest(Verb.GET,
				"https://www.googleapis.com/plus/v1/people/me");
		oAuthService.signRequest(accessToken, request);
	}
	
	public PersonManager getPersonManager() {
		return new PersonManager(oAuthService, accessToken, new PersonJSonParser());
	}
	
	public ActivityManager getActivityManager() {
		return new ActivityManager(oAuthService, accessToken, new ActivityJsonParser());
	}

	public CommentManager getCommentManager() {
		return new CommentManager(oAuthService, accessToken, new CommentJsonParser());
	}
}