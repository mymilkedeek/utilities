package net.mymilkedeek.social.gplus.json;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.mymilkedeek.social.gplus.core.JSonParser;
import net.mymilkedeek.social.gplus.person.Person;

public class PersonJSonParser implements JSonParser<Person> {
	public Person parseJSon(JSONObject json) throws JSONException {
		Person person = new Person();
		person.setId(json.getString("id"));
		person.setDisplayName(json.getString("displayName"));
		{
			JSONObject name = json.getJSONObject("name");
			person.setGivenName(name.getString("givenName"));
			person.setFamilyName(name.getString("familyName"));
		}
		person.setTagline(json.getString("tagline"));
		person.setGender(json.getString("gender"));
		{
			JSONObject image = json.getJSONObject("image");
			person.setImageUrl(image.getString("url"));
		}
		return person;
	}

	public List<Person> parseJSonList(JSONObject json) throws JSONException {
		JSONArray jsonArray = json.optJSONArray("items");
		List<Person> persons = new ArrayList<Person>();
		JSONObject person;
		
		for ( int i = 0; i < jsonArray.length(); i++ ) {
			person = jsonArray.getJSONObject(i);
			persons.add(new Person(person.getString("id"), person.getString("displayName")));
		}
		
		return persons;
	}
}