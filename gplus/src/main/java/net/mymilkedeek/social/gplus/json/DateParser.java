package net.mymilkedeek.social.gplus.json;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class DateParser {
	public static DateTime parseDate(String date) {
		DateTimeFormatter parser = ISODateTimeFormat.dateTime();
        return parser.parseDateTime(date);
	}
}